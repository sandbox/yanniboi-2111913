<?php

/**
 * @file
 */

/**
 * Page callback for the register page.
 */
function biblia_register() {
  return drupal_get_form('biblia_register_form');
}

/**
 * Form for the registration page.
 */
function biblia_register_form($form, &$form_state) {
  $api_key = variable_get('biblia_api', '');

  $form['api_key'] = array(
    '#title' => t('API Key'),
    '#type' => 'textfield',
    '#description' => t('Enter your API key.'),
    '#default_value' => $api_key,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the registration page form.
 */
function biblia_register_form_submit($form, &$form_state) {
  if (isset($form_state['values']['api_key'])) {
    variable_set('biblia_api', $form_state['values']['api_key']);
  }
}

<?php

/**
 * @file
 */

/**
 * Page callback for the data page.
 */
function biblia_data_page() {
  return drupal_get_form('biblia_data_form');
}

/**
 * Rendered form for the data page.
 */
function biblia_data_form($form, &$form_state) {
  if (isset($_SESSION['biblia_reference'])) {
    $form['recent_reference'] = array(
      '#markup' => $_SESSION['biblia_reference'],
    );
  }

  $form['reference'] = array(
    '#title' => t('Bible Reference'),
    '#type' => 'textfield',
    '#description' => t('Enter your bible reference.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/**
 * Submit handler for the data page form.
 */
function biblia_data_form_submit($form, &$form_state) {

  if (isset($form_state['values']['reference'])) {
    $result = biblia_get_reference($form_state['values']['reference']);
  }

  if ($result) {
    $_SESSION['biblia_reference'] = $result->text;
  }
}
